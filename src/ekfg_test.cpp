#include <stdio.h>
#include <ros/ros.h>
#include <kalman_filters/ekfg.hpp>

#define STATES_DIM 6
#define INPUTS_DIM 3

#define MEAS_DEVICES_N 2

typedef enum {MARKER=0, GPS} devices_en;


/*
-States
x
y
z
u
v
w

-Inputs
Fx/m
Fy/m
Fz/m

-Measurments
i) Marker (2 measurements)
z1 = 0.5*x/z
z2 = 0.5*y/z

ii) GPS (3 measurements)
z1 = x
z2 = y
z3 = z
z4 = u
z5 = v
z6 = w
 */

void asctec_system(VectorXd x_prev, VectorXd u,const double dt,VectorXd *x_next)
{
	//Position : Pdot = V
	(*x_next).segment(0,3) =  x_prev.segment(0,3) + dt*x_prev.segment(3,3);

	//Velocity : Vdot = u
	(*x_next).segment(3,3) =  x_prev.segment(3,3) + dt*u;
}

void marker_model(VectorXd x, VectorXd u, VectorXd *y)
{
	double z=x(2);

	if (z<0.01)
		z=0.01;

	(*y)(0) = 0.5*x(0)/z;
	(*y)(1) = 0.5*x(1)/z;
}

void gps_model(VectorXd x, VectorXd u, VectorXd *y)
{
	*y = x;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ekf_test");
	ros::NodeHandle node;

	VectorXd z_m(2);
	VectorXd z_gps(6);
	VectorXd inputs_init(INPUTS_DIM);

	z_gps<<1,2,3,0,0,0;
	z_m <<0.5/3,1.0/3;
	inputs_init << 0,1,0;

	//Initialize

	MatrixXd Pinit;
	Pinit.setIdentity(STATES_DIM,STATES_DIM);

	MatrixXd Qinit;
	Qinit.setIdentity(STATES_DIM,STATES_DIM);
	VectorXd states_init(STATES_DIM);

	MatrixXd Rm;
	Rm.setIdentity(2,2);

	MatrixXd Rgps;
	Rgps.setIdentity(6,6);

	MatrixXd Hgps;
	Hgps.setIdentity(6,6);

	states_init<<0,5,1,0,1,0;

	Vector3d tempA;

	tempA << 1.0,2.0,3.0;

	Vector3d tempB;

	tempB = tempA;

	tempB(0) = 5.0;

	std::cout<< tempA << std::endl;

	//Initialize EKF - Construct System Model
	//System Model, P, Q, states_init, inputs_init, Number of measurement devices, Find F?, two side derivative for both F or H (more accurate)
	ekf ekf_asctec(asctec_system,1000*Pinit,Qinit,states_init,inputs_init,MEAS_DEVICES_N,true,true);


	//Construct Measurement models
	//Index, Measurement model, R, find H?
	ekf_asctec.measurement_ctr(MARKER,marker_model,5.0*Rm,true);

	ekf_asctec.measurement_ctr(GPS,gps_model,2.0*Rgps,false);
	ekf_asctec.update_H(GPS,Hgps);


	//Loop
	bool M_flags[MEAS_DEVICES_N];
	M_flags[GPS]=true;
	M_flags[MARKER]=true;

	ros::Time now = ros::Time::now();

	for (int i=0;i<10;i++)
	{
		M_flags[GPS]=false;
		if (i%5)
			M_flags[GPS]=true;

		std::cout <<""<<std::endl;
		std::cout << "Loop: " << i+1 << std::endl;

		//System Update
		ekf_asctec.update_inputs(inputs_init);
		ekf_asctec.system_update(0.001);
		//std::cout << "x apriori:" << std::endl;
		//std::cout <<  ekf_asctec.states << std::endl;


		//Measurement Update
		ekf_asctec.update_meas(MARKER,0.9*z_m);
		ekf_asctec.update_meas(GPS,z_gps);

		//std::cout <<"H:"<<std::endl;
		//std::cout <<  ekf_asctec.H << std::endl;

		ekf_asctec.measurement_update(M_flags);

		//std::cout << "P:" << std::endl;
		//std::cout <<  ekf_asctec.P << std::endl;

		std::cout << "Trace P=" <<  ekf_asctec.P.trace() << std::endl;

	}
	std::cout << "x:" << std::endl;
	std::cout <<  ekf_asctec.states << std::endl;
	std::cout << "Time: " << 1000.0*((ros::Time::now()-now).toSec())/100.0 << " ms"<< std::endl;

	return 1;
}
