#include <kalman_filters/ekfg.hpp>

struct ClosestSysCmp {
	bool operator()(const system_buf & x, const double & y) { return x.time > y; }
};

struct ClosestMeasCmp {
	bool operator()(const meas_buf & x, const double & y) { return fabs(x.time - y)<0.005; }
};

ekf::ekf(system_func_t fsys,MatrixXd Pinit,MatrixXd Qinit, VectorXd states_init, VectorXd inputs_init, unsigned int nm,bool _findF,bool _acc2nd)
{
	Nx=states_init.rows();
	Nm=nm;

	findF = _findF;

	acc2nd =_acc2nd;

	states = states_init;
	inputs = inputs_init;

	F.setZero(Nx,Nx);

	Q = Qinit;
	P = Pinit;

	sysf = fsys;

	measlots = (obsv_ekfg_measlot_t*) malloc (nm * sizeof (obsv_ekfg_measlot_t));
}

ekf::~ekf()
{
	free(measlots);
	return;
}

void ekf::measurement_ctr(unsigned index,measurement_func_t measf,MatrixXd R,bool findH)
{
	unsigned int nz = R.rows();
	if (index<Nm)
	{
		measlots[index].findH=findH;
		measlots[index].Nz=nz;
		measlots[index].measf=measf;
		measlots[index].H.setZero(nz,Nx);
		measlots[index].z.setZero(nz,1);
		measlots[index].y.setZero(nz,1);
		update_R(index,R);
	}
	else
	{
		std::cout<<"Error! <measurement_ctr> index>Nm"<< std::endl;
		exit(0);
	}
}

void ekf::system_update(double dt)
{
	VectorXd x;
	VectorXd x_old;
	VectorXd y;
	VectorXd ym;

	x = states;
	x_old = states;

	//System propagation
	sysf(states,inputs,dt,&states);

	//Find F
	if (findF)
	{
		y=states;
		ym=states;
		for (int i=0;i<Nx;i++)
		{
			x=x_old;
			x(i)=x_old(i)+TOL;
			sysf(x,inputs,dt,&y);

			if (not acc2nd)
				F.block(0,i,Nx,1)=(y-states)/TOL;
			else
			{	
				x=x_old;
				x(i)=x_old(i)-TOL;
				sysf(x,inputs,dt,&ym);
				F.block(0,i,Nx,1)=(y-ym)/(2*TOL);
			}

		}
	}

	//std::cout << "F:" << std::endl;
	//std::cout << F << std::endl;

	P = F*P*F.transpose()+Q;

}
void ekf::measurement_update(bool *flags)
{
	MatrixXd temp;
	MatrixXd invmat;

	H.resize(0,0);

	VectorXd error_z;
	VectorXd _ez;
	VectorXd tempv;

	bool meas_ok=false;

	unsigned int Nsum=0;
	for (int i=0;i<Nm;i++)
		if (flags[i])
			Nsum+=measlots[i].Nz;


	R.setZero(Nsum,Nsum);

	VectorXd x;
	VectorXd x_old;
	VectorXd y;
	VectorXd ym;

	x = states;
	x_old = states;

	for (int i=0;i<Nm;i++)
	{
		if (flags[i])
		{
			meas_ok=true;
			unsigned int nz = measlots[i].Nz;

			//Run measurement model
			measlots[i].measf(states,inputs,&measlots[i].y);

			if (measlots[i].findH)
			{
				y = measlots[i].y;
				ym = y;
				//Find H
				for (int j=0;j<Nx;j++)
				{
					x=x_old;
					x(j)=x_old(j)+TOL;
					measlots[i].measf(x,inputs,&y);
					if (not acc2nd)
						measlots[i].H.block(0,j,nz,1)=(y-measlots[i].y)/TOL;
					else
					{
						x=x_old;
						x(j)=x_old(j)-TOL;
						measlots[i].measf(x,inputs,&ym);

						measlots[i].H.block(0,j,nz,1)=(y-ym)/(2*TOL);		
					}

				}
			}
			_ez = measlots[i].z-measlots[i].y;

			//Update error z
			tempv = error_z;
			error_z.setZero(error_z.rows()+nz,1);
			error_z << tempv,_ez;

			//Update R
			R.block(tempv.rows(),tempv.rows(),nz,nz)=measlots[i].R;

			//Update H
			temp=H;
			H.setZero(temp.rows()+nz,Nx);

			if (temp.rows() > 0) 
			{
				for (int j=0; j<temp.rows(); j++)
					H.row(j) = temp.row(j);

				for (int j=temp.rows(); j<temp.rows()+nz; j++)
					H.row(j) = measlots[i].H.row(j-temp.rows());
			}
			else 
				H = measlots[i].H;
		}
	}

	if (meas_ok)
	{
		//std::cout << "R:" << std::endl;
		//std::cout << R << std::endl;

		invmat = H*P*H.transpose()+R;

		K = P*H.transpose()*invmat.inverse();

		states = states + K*error_z;

		P = (MatrixXd::Identity(Nx,Nx)-K*H)*P;

	}	
}

void ekf::update_R(unsigned int index,MatrixXd _R)
{
	measlots[index].R=_R;
}

void ekf::update_meas(unsigned int index,VectorXd _z)
{
	measlots[index].z=_z;
}

void ekf::update_Q(MatrixXd _Q)
{
	Q=_Q;
}

void ekf::update_H(unsigned int index,MatrixXd _H)
{
	measlots[index].H=_H;
}

void ekf::update_inputs(VectorXd _inputs)
{
	inputs=_inputs;
}

int ekf::find_near_state(bool *flags)
{
	double min_meas_time = 1000000.0;
	double sys_time = sbuf.back().time;
	double meas_time = 0;

	for (int i=0;i<Nm;i++)
		if (flags[i])
		{
			meas_time = measlots[i].mbuf.back().time;
			if (meas_time<min_meas_time)
				min_meas_time = meas_time;
		}

	std::vector<system_buf>::const_reverse_iterator cri = std::lower_bound(sbuf.rbegin(), sbuf.rend(), min_meas_time, ClosestSysCmp());
	if (cri != sbuf.rend())
	{
		return (int)(cri-sbuf.rbegin());
	}
	return -1;
}

int ekf::find_near_meas(unsigned int index,double sys_time)
{
	std::vector<meas_buf>::const_reverse_iterator cri =
			std::lower_bound(measlots[index].mbuf.rbegin(), measlots[index].mbuf.rend(), sys_time, ClosestMeasCmp());
	if (cri != measlots[index].mbuf.rend())
	{
		return (int)(cri - measlots[index].mbuf.rbegin());
	}
	return -1;
}
