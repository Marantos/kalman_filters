#ifndef EKFG_HPP
#define EKFG_HPP

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <cstdarg>
#include <Eigen/Dense>

#define TOL 1e-5

using namespace Eigen;

typedef void (*system_func_t) (VectorXd x_prev, VectorXd u,const double dt,VectorXd *x_next);
typedef void (*measurement_func_t) (VectorXd x, VectorXd u,VectorXd *y);

struct system_buf
{
	double time;
	VectorXd data_u;
	VectorXd data_x;
	MatrixXd data_P;
};

struct meas_buf
{
	double time;
	VectorXd data_z;
	MatrixXd data_R;
};

struct obsv_ekfg_measlot_t
{
	unsigned int Nz;
	bool findH;
	measurement_func_t measf;
	MatrixXd R;
	MatrixXd H;
	VectorXd z;
	VectorXd y;
	std::vector<meas_buf> mbuf;
};


class ekf
{
public:
	//functions
	ekf(system_func_t,MatrixXd ,MatrixXd , VectorXd, VectorXd ,unsigned int,bool,bool);
	~ekf();
	void measurement_ctr(unsigned int ,measurement_func_t ,MatrixXd, bool );
	void system_update(double);
	void measurement_update(bool *);
	
	void update_R(unsigned int ,MatrixXd);
	void update_meas(unsigned int ,VectorXd);
	void update_H(unsigned int index,MatrixXd);
	void update_Q(MatrixXd);
	void update_inputs(VectorXd);

	int find_near_state(bool *);
	int find_near_meas(unsigned int, double);

	MatrixXd P;
	VectorXd states;
	MatrixXd H;

private:

	//variables
	bool findF;
	bool acc2nd;

	MatrixXd F;
	MatrixXd Q;
	
	MatrixXd R;

	MatrixXd K;

	//VectorXd error_z;
	VectorXd inputs;

	//others
	unsigned int Nx,Nm;

	system_func_t sysf;
	obsv_ekfg_measlot_t *measlots;

	std::vector<system_buf> sbuf;
};

#endif /* EKFG_H */
